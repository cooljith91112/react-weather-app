import React,{ Component } from 'react';

class WeatherTextComponent extends Component{
    constructor(props){
        super(props);
        this.data = props.data;
        console.log("Initialized WeatherTextComponent",props);
    }

    render(){
        return(
            <div>
            <label>Its Working {JSON.stringify(this.data)}</label>
            <img src={"icons/"+this.data.wx_icon} alt="Weather icon"/>
            </div>
        );
    }
}


export default WeatherTextComponent;