import React, {Component} from 'react';
import '../css/map.css';
import GoogleMapReact from 'google-map-react';

class MapComponent extends Component {
    
    static defaultProps = {
    center: [59.938043, 30.337157],
    zoom: 9,
    greatPlaceCoords: {lat: 59.724465, lng: 30.080121}
  };

    componentDidUpdate = (prevProps, prevState)=>{
    // console.log(prevProps,prevState);
  }

    constructor(props){
        super(props);
        this.state = {
            coordinates : {
                lat:0,
                lng:0
            }
        }
    }

    _onMapClick = (e)=>{
        // console.log(e.lat,e.lng);
        this.setState({coordinates : {
            lat:e.lat,
            lng:e.lng
        }});
    }

     _onChildClick = (key, childProps) => {
    this.props.onCenterChange([childProps.lat, childProps.lng]);
  }

  getState(){
      return this.state;
  }
// Then, render it:

    render(){
        return(
            <div id="map">
                 <GoogleMapReact
                        bootstrapURLKeys={{key:"AIzaSyAEU3tbdsvYJHEPwKtsxyLWmyfqtsEip6s"}}
                        defaultCenter={this.props.center}
                        defaultZoom={this.props.zoom}
                        onChildClick={this._onChildClick}
                        onClick={this._onMapClick}
                 >
                </GoogleMapReact>
            </div>
        );
    }

    componentDidMount() {
            //  var map = new google.maps.Map(document.getElementById('map'), {
            //     center: {lat: -34.397, lng: 150.644},
            //     zoom: 8
            // });
       
    }

}

export default MapComponent;