import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Button, Input } from 'semantic-ui-react'
import './App.css';
import superagent from 'superagent';
import WeatherAPI from './weatherapi/WeatherAPI';
import WeatherTextComponent from './WeatherTextComponent';
import MapComponent from './components/MapComponent';



class App extends Component {

  constructor(props){
    super(props);
    

    this.weatherContext = new WeatherAPI({
      apiKey : "560e06ef258771dc88c36cd22f7fd1da",
      appId: "9c7ba32d"
    });
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidUpdate = (prevProps, prevState)=>{
    console.log(prevProps,prevState);
  }

  handleClick(){
    // console.log("Test",superagent);
    let self = this;
    console.log(this.MapComponent.getState());
    superagent
      .get("http://api.weatherunlocked.com/api/forecast/51.50,-0.12?app_id=9c7ba32d&app_key=560e06ef258771dc88c36cd22f7fd1da")
      .end(function(err,resp){
        console.log(self.weatherContext);
        ReactDOM.render(<WeatherTextComponent data={resp.body}/>,document.getElementById('test'))
      })

  }

  mapClick (e){
    console.log("Map Clicked",e.target);
  }

  render() {
    return (
      <div>
        <Input focus placeholder="search.."/>
        <Button primary onClick={this.handleClick}>Search</Button>
        <div id="test"></div>
        <MapComponent></MapComponent>
      </div>
    );
  }

  
}

export default App;
