import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'semantic-ui-css/semantic.min.css';
// import ScriptComponents from './components/ScriptComponents';


// var scriptComponents = new ScriptComponents();

  ReactDOM.render(
    <App />,
    document.getElementById('root')
  );
